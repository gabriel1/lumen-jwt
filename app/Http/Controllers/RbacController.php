<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RbacController extends Controller
{



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware(['role:admin']);
    }

    /**
     * Main
     *
     * @return void
     */

    public function index(){
        
    }  
  

//=== ROLEs CRUD
  
      
    //== GET Lista Roles
    public function getRoles(){
        return Role::all();
    }

    //== GET Lista un rol especifico
    public function getRole($rol){

            return Role::where('name',$rol)->get();
     }
    
    //== POST Crea rol
    public function createRole(Request $data){
        try {
            $rol=Role::create(['name' =>  $data->name]); 
            return $rol;
        } catch (\Exception $e) {
             return array('msg'=>'No se pudo crear el rol');
        }
    }

    //== DELETE Elimina rol
    public function deleteRole($rol){
        try {           
                Role::findByName($rol)->delete();

        } catch (\Exception $e) {
             return array('msg'=>'No se pudo eliminar el rol');
        }        
    }


//=== Relations

    //== POST Asigna permiso a rol
    public function createRelation($rol,Request $req){       
        try {
            $role = Role::findByName($rol);
            $perm = Permission::findByName($req->permission);
            $role->givePermissionTo($perm);

        } catch (\Exception $e) {
             return array('msg'=>'No se pudo relacionar Rol -> Permiso');
        }    
        
    }

    //== DELETE elimna permiso de un rol
    public function deleteRelation($rol,$permission){        

            $rol = Role::findByName($rol);
            $rol->revokePermissionTo($permission);
        
    }


    //== GET checkea permiso en rol 
    public function roleHasPermissionTo($role,$permission){         
        try {
            $role = Role::findByName($role);
            $perm = Permission::findByName($permission);    
            return (Int) $role->hasPermissionTo($perm);

        } catch (\Exception $e) {
             return array('msg'=>'No se pudo relacionar Rol -> Permiso');
        }  

    }

    //== GET relacion
    public function getRelation($rol){              
        try {
            return Role::findByName($rol)->permissions;

        } catch (\Exception $e) {
             return array('error'=>1,'msg'=>'No se pudo encontrar la relacion');
        }  

    }


 //=== Permissions   

    //== GET 
    public function getPermissions(){   
        return Permission::all();
    }

    //== GET 
    public function getPermission($name){
        return Permission::where('name',$name)->get();
    }

    //== POST 
    public function createPermission(Request $req){     
        $list=explode(" ",$req->name);
        $slug=implode('-',$list);
        return Permission::create(['name' => $slug]);
    }

    //== DELETE 
    public function deletePermission($name){
        try {
           return Permission::where('name',$name)->delete();
        } catch (\Exception $e) {
             return array('msg'=>'No se pudo eliminar el permiso');
        }        
    }



//==== User functions 

    //== GET :: Lista usuarios
    public function userList(){
        try {
           $users=User::all();        
           return $users;
        } catch (\Exception $e) {
             return array('msg'=>'No se ha podido listar los usuarios');
        } 
    }   

    //== POST :: Nuevo usuario
    public function createUser(Request $request){

        try {
           $this->validate($request, [
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            ]);
           
           $pass=app('hash')->make($request['password']);
           $data=array('email'=>$request['email'],'name'=>$request['name'],'lastname'=>$request['lastname'],'password'=>$pass);   

           return User::create($data); 
        } catch (\Exception $e) {
             return array('msg'=>'No se ha podido crear el usuario');
        } 
    }   

    //== PUT :: Update user
    public function updateUser($id,Request $request){
           $this->validate($request, [
            'name' => 'string|max:255',
            'lastname' => 'string|max:255',
            'email' => 'string|email|max:255|unique:users',
            'password' => 'string|min:6',
            ]);
                                   
            $user = User::find($id);

            if ($request->has('name')) {
                $user->name=$request->name;
            }
            if ($request->has('lastname')) {
                $user->lastname=$request->lastname;
            }     
            if ($request->has('email')) {
                $user->email=$request->email;
            }     
            if ($request->has('password')) {
                $user->password=app('hash')->make($request->password); 
            }
            $user->save();
        

     } 

    //== PUT :: Update user
    public function updateUserProfile(Request $request){

           $this->validate($request, [
            'name' => 'string|max:255',
            'lastname' => 'string|max:255',
            // 'email' => 'string|email|max:255|unique:users',
            'password' => 'string|min:6',
            ]);
            $user = Auth::user();

            if ($request->has('name')) {
                $user->name=$request->name;
            }
            if ($request->has('lastname')) {
                $user->lastname=$request->lastname;
            }        
            if ($request->has('password')) {
                $user->password=app('hash')->make($request->password); 
            }
          
           $user->save();

           return array('msg'=>'Perfil Actualizado');

     } 

    //== DELETE :: Eliminar usuario
    public function deleteUser($id){
        try {
           
        $user= Auth::user();
        $userID= Auth::ID();

        if(!$user->hasPermissionTo('rbac-delete-users') || $userID==$id)
            throw new \Exception;
            
            return User::destroy($id);

        } catch (\Exception $e) {
             return array('msg'=>'Ud. no tiene permisos para eliminar un usuario');
        } 
    }   


    //== GET :: Lista los roles del usuario
    public function userListRoles($userID){

        try {
            $user=User::where('id', $userID)->first();
            return $user->getRoleNames();  
        } catch (\Exception $e) {
             return 0;
        } 
    }       

    //== GET :: Lista los permisos del usuario
    public function userListPermissions($userID){

            $user=User::where('id', $userID)->first();
            return $user->getAllPermissions();   

    }   

    //== GET :: El usuario tienen este rol? 
    public function userHasRole($userID, $role){     
            $user=User::where('id', $userID)->first();
            return (int) $user->hasRole($role);
    }  

    

    //== POST :: Asigna Rol
    public function userAssignRole($id, Request $req){
             $user=User::where('id', $id)->first();
             return $user->assignRole($req['rol']);
    }  

    //== DELETE :: Elimina Rol
    public function userRemoveRole($userID, $role){
        
            $user=User::where('id', $userID)->first();
            $user->removeRole($role);

    } 

    //== DELETE :: Elimina Rol
    public function currentUser(){
            $user=Auth::user();
            $user->isAdmin=$user->hasRole('admin');;
             return $user;

    } 


//==== Private Helpers

    public function test(){

        $oid=DB::table('roles')->insertGetId([
            'name' => 'super3',
            'guard_name' => 'api',
        ]);
           
        $uid=DB::table('users')->insertGetId([
            'name' => 'admin3',
            'email' => 'admin2@admin.com',
            'password' => bcrypt('123456'),
            'role_ids' => array($oid)

        ]);

        DB::table('roles')
        ->where('_id',$uid)
        ->update(['user_ids' => $uid]);



        // $user= Auth::user();

        // if($user->hasPermissionTo('use-rbac'))
        //     return 1;
        // else
        //     return 0;
 
    }


}
