<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$api = $app->make(Dingo\Api\Routing\Router::class);

$api->version('v1', function ($api) {
    $api->post('/auth/login', [
        'as' => 'api.auth.login',
        'uses' => 'App\Http\Controllers\Auth\AuthController@postLogin',
    ]);


    //=== RBAC Group ADMIN
    $api->group([
        'middleware' => ['api.auth','role:admin'],
    ], function ($api) {

        // ==== RBAC : Roles

        $api->get('/rbac/roles', [
            'uses' => 'App\Http\Controllers\RbacController@getRoles',
            'as' => 'api.rbac.roles'
        ]);
        $api->get('/rbac/roles/{id}', [
            'uses' => 'App\Http\Controllers\RbacController@getRole',
            'as' => 'api.rbac.roles'
        ]);
        $api->post('/rbac/roles', [
        'uses' => 'App\Http\Controllers\RbacController@createRole',
        'as' => 'api.rbac.roles'
         ]);
        $api->delete('/rbac/roles/{id}', [
        'uses' => 'App\Http\Controllers\RbacController@deleteRole',
        'as' => 'api.rbac.roles'
         ]);

         // ==== RBAC : Permissions

        $api->get('/rbac/permissions', [
            'uses' => 'App\Http\Controllers\RbacController@getPermissions',
            'as' => 'api.rbac.permissions'
        ]);
        $api->get('/rbac/permissions/{id}', [
            'uses' => 'App\Http\Controllers\RbacController@getPermission',
            'as' => 'api.rbac.permissions'
        ]);
        $api->post('/rbac/permissions', [
            'uses' => 'App\Http\Controllers\RbacController@createPermission',
            'as' => 'api.rbac.permissions'
        ]);
        $api->delete('/rbac/permissions/{id}', [
            'uses' => 'App\Http\Controllers\RbacController@deletePermission',
            'as' => 'api.rbac.permissions'
        ]);

        // ==== RBAC : Relations

        $api->post('/rbac/relations/{rol}', [
            'uses' => 'App\Http\Controllers\RbacController@createRelation',
            'as' => 'api.rbac.relations'
        ]);

        $api->get('/rbac/relations/{rol}', [
            'uses' => 'App\Http\Controllers\RbacController@getRelation',
            'as' => 'api.rbac.relations'
        ]);

        $api->get('/rbac/relations/{rol}/{perm}', [
            'uses' => 'App\Http\Controllers\RbacController@roleHasPermissionTo',
            'as' => 'api.rbac.relations'
        ]);
                
        $api->delete('/rbac/relations/{rol}/{perm}', [
            'uses' => 'App\Http\Controllers\RbacController@deleteRelation',
            'as' => 'api.rbac.relations'
        ]);


        // ==== RBAC : Users
        $api->get('/rbac/users', [
            'uses' => 'App\Http\Controllers\RbacController@userList',
            'as' => 'api.rbac.users'
        ]);

        $api->post('/rbac/users', [
            'uses' => 'App\Http\Controllers\RbacController@createUser',
            'as' => 'api.rbac.users'
        ]);

        $api->put('/rbac/users/{id}', [
            'uses' => 'App\Http\Controllers\RbacController@updateUser',
            'as' => 'api.rbac.users'
        ]);

        $api->delete('/rbac/users/{id}', [
            'uses' => 'App\Http\Controllers\RbacController@deleteUser',
            'as' => 'api.rbac.users'
        ]);

        $api->get('/rbac/users/{id}/roles', [
            'uses' => 'App\Http\Controllers\RbacController@userListRoles',
            'as' => 'api.rbac.users.roles'
        ]);

        $api->post('/rbac/users/{id}/roles', [
            'uses' => 'App\Http\Controllers\RbacController@userAssignRole',
            'as' => 'api.rbac.users.roles'
        ]);

        $api->delete('/rbac/users/{id}/roles/{rol}', [
            'uses' => 'App\Http\Controllers\RbacController@userRemoveRole',
            'as' => 'api.rbac.users'
        ]);

        $api->get('/rbac/users/{id}/permissions', [
            'uses' => 'App\Http\Controllers\RbacController@userListPermissions',
            'as' => 'api.rbac.users.permissions'
        ]);

    });



    $api->group([
        'middleware' => 'api.auth',
    ], function ($api) {
        $api->get('/', [
            'uses' => 'App\Http\Controllers\APIController@getIndex',
            'as' => 'api.index'
        ]);
        $api->get('/auth/user', [
            'uses' => 'App\Http\Controllers\Auth\AuthController@getUser',
            'as' => 'api.auth.user'
        ]);
        $api->patch('/auth/refresh', [
            'uses' => 'App\Http\Controllers\Auth\AuthController@patchRefresh',
            'as' => 'api.auth.refresh'
        ]);
        $api->delete('/auth/invalidate', [
            'uses' => 'App\Http\Controllers\Auth\AuthController@deleteInvalidate',
            'as' => 'api.auth.invalidate'
        ]);


        // ==== Profile

        $api->put('/rbac/users', [
            'uses' => 'App\Http\Controllers\RbacController@updateUserProfile',
            'as' => 'api.rbac.users'
        ]);

        $api->get('/profile/me', [
            'uses' => 'App\Http\Controllers\RbacController@currentUser',
            'as' => 'api.auth.user'
        ]);


       


    });


});

